public with sharing class SignatureHelper {

    @AuraEnabled
    public static Attachment saveSign(String strSignElement, Id contactId){
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Signature ' + Datetime.now();
        objAttachment.ParentId = contactId;
        objAttachment.ContentType = 'image/png';
        objAttachment.Body = EncodingUtil.base64Decode(strSignElement);
        insert objAttachment;
        return objAttachment;
    }
}