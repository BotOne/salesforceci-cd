//package com.gbc.triggerservice
/**
 * @author jevgenisa
 * @description base class to be extended with prefiltering logic before the execution of core business logic.
 */
public abstract class TriggerService {
    /**
     * @param newObj new SObject
     * @param field field to compare with old SObject
     *
     * @return result depending on whether the field was changed, always returns true if oldMap is empty i.e this is a insert operation
     */
    @TestVisible
    protected virtual Boolean isFieldUpdated(SObject newObj, SObjectField field){
        return getOldRecordsMap().isEmpty() ? true : getOldRecordsMap().get(newObj.Id).get(field)!=newObj.get(field);
    }

    protected virtual Map<Id,SObject> getOldRecordsMap(){
        return Trigger.oldMap !=null ? Trigger.oldMap : new Map<Id, SObject>();
    }

    protected virtual void addFieldError(SObject obj, SObjectField field, String error){
        obj.addError(field, error);
    }

}