@isTest
public with sharing class LeadProcessorTest {
    @TestSetup
    public static void init(){
        List<Lead> leads = RandomGenerateFactory.generateLeads(200);
        insert leads;
    }

    @IsTest
    public static void batchTest(){
        Test.startTest();
        LeadProcessor processor = new LeadProcessor();
        Database.executeBatch(processor);
        Test.stopTest();

        List<Lead> leads = [SELECT LeadSource FROM Lead];
        for(Lead lead : leads){
            System.assert(lead.LeadSource == 'Dreamforce');
        }
    }
}