//package com.gbc.business
/**
 * Created by jevgenisa on 07.10.2021.
 */

public class SObjectValidationError {
    public SObject obj;
    public SObjectField field;
    public String error;

    public SObjectValidationError(SObject obj, SObjectField field, String error){
        this.obj=obj;
        this.error=error;
        this.field=field;
    }
}