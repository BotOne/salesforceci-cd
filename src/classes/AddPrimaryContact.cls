/**
 * Created by BotOne on 26.12.2019.
 */

public with sharing class AddPrimaryContact implements Queueable {

    private Contact contact;
    private String state;
    public AddPrimaryContact(Contact contact, string state){
        this.contact = contact;
        this.state = state;
    }

    public void execute(QueueableContext param1) {
        List<Account> accounts = [
                SELECT Id, Name, (SELECT Id, FirstName, LastName from contacts)
                FROM Account
                WHERE BillingState = :state
                LIMIT 200
        ];

        List<Contact> contacts = new List<Contact>();
        for(Account account: accounts){
            Contact newContact = contact.clone();
            newContact.AccountId = account.Id;
            newContact.LastName = 'Auto Create Name';
            contacts.add(newContact);
        }
        insert contacts;
    }
}