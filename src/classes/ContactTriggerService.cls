public class ContactTriggerService extends TriggerService{
    public void showError(Contact record, SObjectField field, String message){
        addFieldError(record, field, message);
    }
}