public class MapController
{
    // System Attributes
    public static String statusMessage {get; set;}

    @RemoteAction
    public static Account getAccount(){
        return [SELECT Id, Name, Address__c, Address__Latitude__s, Address__Longitude__s,
        (SELECT Id, Name FROM Routes__r) FROM Account LIMIT 1];
    }

    @RemoteAction
    public static Plan__c getPlan(String accountId){
        return [SELECT Id, Name, Account__c, StartPoint__c,
        (SELECT Id, Name, Account__c, CheckInDate__c, CheckOutDate__c, Status__c FROM Visits__r) FROM Plan__c
        WHERE Account__c = :accountId LIMIT 1];
    }

    @RemoteAction
    public static List<Account> getAccounts(){
        return [SELECT Id, Name, Address__c, Address__Latitude__s, Address__Longitude__s,
        (SELECT Id, Name FROM Routes__r) FROM Account];
    }

    

    @RemoteAction
    public static Visit__c checkIn(String visitId){
        List<Visit__c> visits = [SELECT Id, Status__c, CheckInDate__c, CheckOutDate__c FROM Visit__c WHERE Id = :visitId];
        if(visits == null || visits.size() == 0){
            statusMessage = 'ERROR: Visit "' + visitId + '" not found';
            return null;
        }
        Visit__c visit = visits[0];
        visit.CheckInDate__c = Datetime.now();
        visit.Status__c = 'Plan';

        update visit;

        statusMessage = 'SUCCESS: Visit "'+ visitId + '"check in was updated.';
        return visit;
    }

    @RemoteAction
    public static Visit__c checkOut(String visitId){
        List<Visit__c> visits = [SELECT Id, Status__c, CheckInDate__c, CheckOutDate__c FROM Visit__c WHERE Id = :visitId];
        if(visits == null || visits.size() == 0){
            statusMessage = 'ERROR: Visit "' + visitId + '" not found';
            return null;
        }
        Visit__c visit = visits[0];
        visit.CheckOutDate__c = Datetime.now();
        visit.Status__c = 'Complete';

        update visit;

        statusMessage = 'SUCCESS: Visit "'+ visitId + '" check out was updated.';
        return visit;
    }

    @RemoteAction
    public static Plan__c setStartPoint(String planId, Double lat, Double lng){
        Plan__c plan = [SELECT Id, Name, StartPoint__c, StartPoint__Latitude__s, StartPoint__Longitude__s
        FROM Plan__c Where Id = :planId LIMIT 1];

        if(plan == null){
            statusMessage = 'ERROR: Plan not set';
            return null;
        }

        plan.StartPoint__Latitude__s = lat;
        plan.StartPoint__Longitude__s = lng;

        update plan;

        statusMessage = 'SUCCES: Plan '+ planId +' set correct.';
        return plan;
    }
}