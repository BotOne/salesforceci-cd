/**
 * Created by BotOne on 13.10.2019.
 */

public with sharing class StringArrayTest {

    static public List<String> generateStringArray(Integer count){
        List<String> result = new List<String>();
        for(Integer index = 0; index < count; ++index){
            String str = 'Test ' + index;

            result.add(str);
        }
        return result;
    }
}