public with sharing class ContactTriggerHandler extends TriggerHandler implements BeforeUpdate {
    public void beforeUpdate(List<Contact> newRecords, List<Contact> oldRecords) {
        Contact record = newRecords[0];
        new ContactTriggerService().showError(record, Contact.Description, 'Test Error');
    }
}