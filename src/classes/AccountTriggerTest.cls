@isTest
private class AccountTriggerTest {
    @isTest
    static void testCreateNewAccount() {
        // Test Setup data
        // Create 200 new Accounts
        List<Account> accounts = new List<Account>();
        for(Integer i=0; i < 200; i++) {
            Account acct = new Account(Name ='Account_' + i, BillingState = 'CA');
            accounts.add(acct);
        }
        // Perform Test
        Test.startTest();
        insert accounts;
        Test.stopTest();

        // Verify that 200 new Accounts were inserted
        List<Account> verifyAccounts = [SELECT BillingState, ShippingState FROM Account];
        System.assertEquals(200, verifyAccounts.size());

        // Verify that accounts have correct 'BillingState'
        for(Account account : verifyAccounts){
            System.assertEquals(account.BillingState, account.ShippingState);
        }
    }
}