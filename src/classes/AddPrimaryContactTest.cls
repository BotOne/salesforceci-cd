@isTest
public class AddPrimaryContactTest {


    @isTest static void testList(){
        List<Account> testAccount = new List <Account>();
        for(Integer i=0;i<50;i++){

            testAccount.add(new Account(BillingState = 'CA', name = 'Test'+i));
        }
        for(Integer j=0;j<50;j++){

            testAccount.add(new Account(BillingState = 'NY', name = 'Test'+j));

        }
        insert testAccount;
        Contact co = new Contact();
        String state = 'CA';
        AddPrimaryContact apc = new AddPrimaryContact(co, state);
        Test.startTest();
        System.enqueueJob(apc);
        Test.stopTest();

        System.assert (apc != null);
    }
}