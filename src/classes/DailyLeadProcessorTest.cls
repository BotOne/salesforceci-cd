@isTest
private class DailyLeadProcessorTest{
    //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    public static String cron = '0 0 0 2 6 ? 2022';

    @IsTest
    static void testScheduledJob(){
        List<Lead> leads = new List<Lead>();

        for(Integer i = 0; i < 200; i++){
            Lead lead = new Lead(LastName = 'Test ' + i, LeadSource = '', Company = 'Test Company ' + i, Status = 'Open - Not Contacted');
            leads.add(lead);
        }

        insert leads;

        Test.startTest();
        // Schedule the test job
        System.schedule('Update LeadSource to DreamForce', cron, new DailyLeadProcessor());

        // Stopping the test will run the job synchronously
        Test.stopTest();

        Lead lead = [SELECT LeadSource FROM Lead LIMIT 1];
        System.assert(lead.LeadSource == 'Dreamforce');
    }
}