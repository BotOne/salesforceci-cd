//package com.gbc.business
/**
 * Created by jevgenisa on 07.10.2021.
 */

public interface SObjectValidator {

    List<SObjectValidationError> validate(List<SObject> objects);
}