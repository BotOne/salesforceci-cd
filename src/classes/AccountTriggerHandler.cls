public with sharing class AccountTriggerHandler {
    public static void createAccounts(List<Account> accounts){
        for(Account account : accounts){
            if(account.ShippingState != account.BillingState){
                account.ShippingState = account.BillingState;
            }
        }
    }
}