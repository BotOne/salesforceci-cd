@IsTest
public with sharing class AccountProcessorTest {

    @TestSetup
    public static void init(){
        RandomGenerateFactory.generateAccounts(1);
    }

    @IsTest
    public static void countContactsTest()
    {
        Test.StartTest();

        Account account = new Account(Name = 'Test Account');
        insert account;

        Contact contact = new Contact(
                FirstName = 'First Name',
                LastName = 'Last Name',
                AccountId = account.Id);
        insert contact;

        Test.StopTest();

        List<Id> ids = new List<Id>(new Map<Id, Account>([select Id from Account]).keySet());
        AccountProcessor.countContacts(ids);

        System.assert (ids != null);
    }
}