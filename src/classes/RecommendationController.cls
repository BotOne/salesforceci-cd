global with sharing class RecommendationController {
    global class DataContainer {
        @InvocableVariable
        public string accountId;
    }

    @InvocableMethod(label='Accounts for recommendations' description='Recommend accounts the current user should.')
    global static List<List<Recommendation>> getAccounts(List<DataContainer> inputData){
        List<List<Recommendation>> outputs = new List<List<Recommendation>>();
        Integer daysSinceLastContact;
        Account[] accounts = [SELECT Name, Description, OwnerId FROM Account LIMIT 10];
        List<Recommendation> recs = new List<Recommendation>();
        for (Account account:accounts) {
            Recommendation rec = new Recommendation(
                    Name = account.Name,
                    Description = 'Connect with the ' + account.Name + ' account, the last interaction was '+ daysSinceLastContact + ' days ago.',
                    //Pre-req: Create a screen flow with the name simpleFlow
                    ActionReference = 'simpleFlow',
                    AcceptanceLabel = 'View'
            );
            recs.add(rec);
        }
        outputs.add(recs);
        return outputs;
    }
}