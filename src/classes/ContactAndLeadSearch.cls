public with sharing class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String value){
        List<List<SObject>> result = [FIND :value IN ALL FIELDS
                RETURNING Contact(FirstName, LastName), Lead(FirstName,LastName)];
        return result;
    }

}