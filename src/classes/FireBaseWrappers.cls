public with sharing class FireBaseWrappers {
    public class Root{
        public String userUid; // тип string, обязательное поле
        public String title; // тип string, обязательное поле
        public String status; // тип string, обязательное поле, один из APPOINTED, DONE, APPROVED, CANCELED
        public String dueDate; // тип string, обязательное поле
        public String description; // тип string, обязательное поле, может быть HTML
        public Integer score; // тип number, обязательное поле
        public AnswerWrapper answers; 
    }

    public class AnswerWrapper{
        public String  type; // тип string, обязательное поле
        public Map<String, Map<String, RecognizeItemWrapper>> itemsToRecognize; //список товаров для распознавания
    }

    public class RecognizeItemWrapper{
        public String id; //ид, прилетающий с бэка
        public String label; //название товара, которое будет показано пользователю
        public String status;// back. тип string. Статус распознования - дублируется после расспознования фотографий, значение при создании - "not found"
    }
}