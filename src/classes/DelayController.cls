/**
 * Created by BotOne on 26.08.2019.
 */

global class DelayController implements Schedulable { 
    public static final Integer JOB_DELAY = 5;
    public static final String JOB_NAME = 'Update_Leads';

    public void execute(SchedulableContext context) {
        String jobId = context.getTriggerId();
        List<CronTrigger> jobs = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType
        FROM CronTrigger
        WHERE Id = :jobId];

        for(CronTrigger job : jobs){
            System.abortJob(job.Id);
            System.debug('Schedule Task: ' + job.Id + ' is complete.');
        }

        List<Lead> leads = [SELECT Id, Convertion_rate_score__c FROM Lead WHERE Convertion_rate_score__c = ''];
        for(Lead lead : leads){
            if(String.isEmpty(lead.Convertion_rate_score__c)){
                lead.Convertion_rate_score__c = '0';
            }
        }

        //update leads;
        System.debug('Complete Update Leads:' + leads);
    }

    public static void startLeadsUpdate(){
        Datetime eventDate = Datetime.now().addSeconds(JOB_DELAY);
        String hour = String.valueOf(eventDate.hour());
        String min = String.valueOf(eventDate.minute());
        String ss = String.valueOf(eventDate.second());
        String eventTime = ss + ' ' + min + ' ' + hour + ' * * ?';

        DelayController controller = new DelayController();

        System.schedule(JOB_NAME, eventTime, controller);
        System.debug('Start Update Leads');
    }
}