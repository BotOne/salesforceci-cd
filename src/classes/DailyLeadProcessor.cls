/**
 * Created by BotOne on 27.12.2019.
 */

public with sharing class DailyLeadProcessor implements Schedulable {


    public void execute(SchedulableContext context) {
        List<Lead> leads = [SELECT Id, LeadSource FROM Lead WHERE (LeadSource = null OR LeadSource = '') LIMIT 200];
        for(Lead lead : leads){
            lead.LeadSource = 'Dreamforce';
        }
        update leads;
    }
}