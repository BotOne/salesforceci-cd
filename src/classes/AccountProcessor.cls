public with sharing class AccountProcessor {

    @future
    public static void countContacts(List<Id> accountIds)
    {
        List<Account> accounts = new List<Account>();
        for(AggregateResult result : [
                SELECT AccountId a, COUNT(id) c
                FROM Contact
                WHERE AccountId IN :accountIds
                GROUP BY AccountId]){

            Account newAccount = new Account(Id = (Id)result.get('a'), Number_of_Contacts__c = (Integer)result.get('c'));
            accounts.add(newAccount);
        }

        update accounts;
    }
}