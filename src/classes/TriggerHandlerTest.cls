//package com.gbc.triggerhandler.test
/**
 * Created by jevgenisa on 14.05.2020.
 */

@IsTest
public class TriggerHandlerTest {

    static Integer orderNo=0;

    class TriggerHandlerMock extends TriggerHandler implements BeforeInsert,BeforeUpdate,BeforeDelete,AfterInsert,AfterUpdate,AfterDelete,AfterUndelete{
        Boolean beforeInsertCalled=false;
        Boolean afterInsertCalled=false;
        Boolean beforeUpdateCalled=false;
        Boolean afterUpdateCalled=false;
        Boolean beforeDeleteCalled=false;
        Boolean afterDeleteCalled=false;
        Boolean afterUndeleteCalled=false;

        TriggerHandlerMock(TriggerStep triggerStep, List<SObject> newRecords, List<SObject> oldRecords, Map<Id,SObject> newRecordsMap, Map<Id,SObject> oldRecordsMap){
            super(triggerStep, newRecords, oldRecords, newRecordsMap, oldRecordsMap);
        }

        public void beforeInsert(List<SObject> newRecords){
            beforeInsertCalled=true;
        }


        public void afterInsert(List<SObject> newRecords){
            afterInsertCalled=true;
        }


        public void beforeUpdate(List<SObject> newRecords, List<SObject> oldRecords){
            beforeUpdateCalled=true;
        }


        public void afterUpdate(List<SObject> newRecords, List<SObject> oldRecords){
            afterUpdateCalled=true;
        }


        public void beforeDelete(List<SObject> oldRecords){
            beforeDeleteCalled=true;
        }


        public void afterDelete(List<SObject> oldRecords){
            afterDeleteCalled=true;
        }


        public void afterUndelete(List<SObject> newRecords){
            afterUndeleteCalled=true;
        }
    }


    private static Account createAccount(){
        Account a = new Account(Name='Test'+orderNo, AccountNumber='1234'+orderNo++);
        insert a;
        return a;
    }

    @IsTest
    public static void beforeInsertShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isBefore=true;
        step.isInsert=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.beforeInsertCalled);
    }

    @IsTest
    public static void afterInsertShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isInsert=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.afterInsertCalled);
    }

    @IsTest
    public static void beforeUpdateShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isBefore=true;
        step.isUpdate=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.beforeUpdateCalled);
    }

    @IsTest
    public static void afterUpdateShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isUpdate=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.afterUpdateCalled);
    }

    @IsTest
    public static void beforeDeleteShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isBefore=true;
        step.isDelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.beforeDeleteCalled);
    }

    @IsTest
    public static void afterDeleteShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isDelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.afterDeleteCalled);
    }

    @IsTest
    public static void afterUndeleteShouldBeCalled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isUndelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(mock.afterUndeleteCalled);
    }

    @IsTest
    public static void preventExecutionWorks(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isDelete=true;
        TriggerHandler.preventExecution=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        mock.handle();
        System.assert(!mock.afterDeleteCalled);
    }

    @IsTest
    public static void skipsExecutionIfAllTriggersAreDisabled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isDelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        TriggerHandler.disabledTriggerHandlers=new Set<String>{'All'};
        System.assertEquals(true, mock.triggerHandlerClassIsDisabled('afterDelete'));
    }

    @IsTest
    public static void skipsExecutionIfTriggerHandlerClassIsDisabled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isDelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        TriggerHandler.disabledTriggerHandlers=new Set<String>{'TriggerHandlerMock'};
        System.assertEquals(true, mock.triggerHandlerClassIsDisabled('afterDelete'));
    }

    @IsTest
    public static void skipsExecutionIfTriggerHandlerMethodIsDisabled(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isDelete=true;
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{new Account()}, new List<SObject>{new Account()}, null, null);
        TriggerHandler.disabledTriggerHandlers=new Set<String>{'TriggerHandlerMock_afterDelete'};
        System.assertEquals(true, mock.triggerHandlerClassIsDisabled('afterDelete'));
    }

    @IsTest
    public static void isFieldUpdatedShouldReturnFalse(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isUpdate=true;
        Account acc = createAccount();
        Account newAccount = acc.clone(true);
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{newAccount}, new List<SObject>{acc}, new Map<Id,SObject>{newAccount.Id=>newAccount},new Map<Id,SObject>{acc.Id=>acc});
        System.assertEquals(false, mock.isFieldUpdated(newAccount, Account.Name));
    }

    @IsTest
    public static void isFieldUpdatedShouldReturnTrue(){
        TriggerHandler.TriggerStep step = new TriggerHandler.TriggerStep();
        step.isAfter=true;
        step.isUpdate=true;
        Account acc = createAccount();
        Account newAccount = acc.clone(true);
        newAccount.Name=acc.Name+' new';
        TriggerHandlerMock mock =new TriggerHandlerMock(step,new List<SObject>{newAccount}, new List<SObject>{acc}, new Map<Id,SObject>{newAccount.Id=>newAccount},new Map<Id,SObject>{acc.Id=>acc});
        System.assertEquals(true, mock.isFieldUpdated(newAccount, Account.Name));
    }


}