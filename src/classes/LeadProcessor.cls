public with sharing class LeadProcessor implements Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext param1) {
        return Database.getQueryLocator(
                'SELECT LeadSource FROM Lead'
        );
    }

    public void execute(Database.BatchableContext bc, List<Lead> scope) {
        List<Lead> leads = new List<Lead>();
        for(Lead lead : scope){
            lead.LeadSource = 'Dreamforce';
            leads.add(lead);
        }
        update leads;
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('Batch Complete - ' + bc); 
    }
}