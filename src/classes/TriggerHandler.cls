//package com.gbc.triggerhandler

/**
 * @author jevgenisa
 * @description base class to be extended with SObject specific trigger handlers.
 * In order to handle a specific trigger step a dedicated interface should be implemented
 * Trigger execution can be prevented either from code using preventExecution static variable or from Custom metadata
 * Custom metadata code execution prevention can be done creating TriggerDisabledHandler__mdt with setting right DeveloperName in following ways:
 * Using DeveloperName = All disables all the triggers
 * Using DeveloperName = <TriggerHandler> ,e.g AccountTriggerHandler disables all the triggers for this TriggerHandler class
 * Using DeveloperName = <TriggerHandler>_<handlerMethod>, e.g AccountTriggerHandler_beforeInsert disables execution for the given class method
 */
public abstract class TriggerHandler {
    private static String disableMdt ='TriggerDisabledHandler__mdt';


    public static Boolean preventExecution = false;
    @TestVisible
    private static Set<String> disabledTriggerHandlers = new Set<String>();

    private TriggerStep triggerStep;
    private List<SObject> newRecords;
    private List<SObject> oldRecords;
    private Map<Id, SObject> newRecordsMap;
    private Map<Id, SObject> oldRecordsMap;

    public class TriggerHandlerException extends Exception {}

    @TestVisible
    protected TriggerHandler(){
        this.triggerStep = new TriggerStep();
        this.newRecords=Trigger.new;
        this.oldRecords=Trigger.old;
        this.newRecordsMap=Trigger.newMap;
        this.oldRecordsMap=Trigger.oldMap;
    }

    @TestVisible
    private TriggerHandler(TriggerStep triggerStep, List<SObject> newRecords, List<SObject> oldRecords, Map<Id,SObject> newRecordsMap, Map<Id,SObject> oldRecordsMap){
        this.triggerStep=triggerStep;
        this.newRecords=newRecords;
        this.oldRecords=oldRecords;
        this.newRecordsMap=newRecordsMap;
        this.oldRecordsMap=oldRecordsMap;
    }

    @TestVisible
    private class TriggerStep {
        @TestVisible private Boolean isBefore=Trigger.isBefore==true;
        @TestVisible private Boolean isAfter=Trigger.isAfter==true;
        @TestVisible private Boolean isInsert=Trigger.isInsert==true;
        @TestVisible private Boolean isUpdate=Trigger.isUpdate==true;
        @TestVisible private Boolean isDelete=Trigger.isDelete==true;
        @TestVisible private Boolean isUndelete=Trigger.isUndelete==true;

        @TestVisible private String getTriggerHandlerMethodName(){
            return (isBefore ? 'before' : 'after') + (isInsert ? 'Insert' : (isUpdate ? 'Update' : (isDelete ? 'Delete' : 'Undelete')));
        }

    }


    public interface BeforeInsert {
        void beforeInsert(List<SObject> newRecords);
    }
    public interface AfterInsert {
        void afterInsert(List<SObject> newRecords);
    }

    public interface BeforeUpdate {
        void beforeUpdate(List<SObject> newRecords, List<SObject> oldRecords);
    }

    public interface AfterUpdate {
        void afterUpdate(List<SObject> newRecords, List<SObject> oldRecords);
    }

    public interface BeforeDelete {
        void beforeDelete(List<SObject> oldRecords);
    }

    public interface AfterDelete {
        void afterDelete(List<SObject> oldRecords);
    }

    public interface AfterUndelete {
        void afterUndelete(List<SObject> newRecords);
    }

    static{
        if(!Schema.getGlobalDescribe().containsKey(disableMdt)){
            return;
        }
        List<SObject> disabledHandlerMdts= Database.query('SELECT DeveloperName FROM '+ disableMdt);
        for(SObject mdt : disabledHandlerMdts){
            disabledTriggerHandlers.add((String)mdt.get('DeveloperName'));
        }
    }
    /**
     *
     * @description this is a triggerhandler registration method that must be called from .trigger file
     * @example
     * trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
     *  new AccountTriggerHandler().handle();
     * }
     */
    public void handle(){
        if (preventExecution || triggerHandlerClassIsDisabled(triggerStep.getTriggerHandlerMethodName())){
            return;
        }

        if (triggerStep.isBefore && triggerStep.isInsert && this instanceof BeforeInsert) {
            ((BeforeInsert) this).beforeInsert(newRecords);
        }

        if (triggerStep.isAfter && triggerStep.isInsert && this instanceof AfterInsert) {
            ((AfterInsert)this).afterInsert(newRecords);
        }

        if (triggerStep.isBefore && triggerStep.isUpdate && this instanceof BeforeUpdate) {
            ((BeforeUpdate)this).beforeUpdate(newRecords, oldRecords);
        }

        if (triggerStep.isAfter && triggerStep.isUpdate && this instanceof AfterUpdate) {
            ((AfterUpdate)this).afterUpdate(newRecords, oldRecords);
        }

        if (triggerStep.isBefore && triggerStep.isDelete && this instanceof BeforeDelete) {
            ((BeforeDelete)this).beforeDelete(oldRecords);
        }

        if (triggerStep.isAfter && triggerStep.isDelete && this instanceof AfterDelete) {
            ((AfterDelete)this).afterDelete(oldRecords);
        }

        if (triggerStep.isAfter && triggerStep.isUndelete && this instanceof AfterUndelete) {
            ((AfterUndelete)this).afterUndelete(newRecords);
        }

    }

    @TestVisible
    private Boolean triggerHandlerClassIsDisabled(String triggerHandlerMethodName){
        if (disabledTriggerHandlers.contains('All')){
            return true;
        }

        String thisClassName = String.valueOf(this).split(':')[0];
        if (disabledTriggerHandlers.contains(thisClassName)){
            return true;
        }

        if (disabledTriggerHandlers.contains(thisClassName+'_'+triggerHandlerMethodName)){
            return true;
        }

        return false;
    }

    /**
     * @param obj new SObject to check
     * @param field field to check
     *
     * @return true if new object field differs from Trigger.old. If in Update trigger step then true is always returned
     * @example
     * beforeUpdate(List<Account> newRecords, List<Account> oldRecords){
     *  for(Account newRecord :newRecords){
     *      if(isFieldUpdated(newRecord, Account.Name)){
     *          //do something...
     *      }
     *
     *  }
     * }
     */
    @TestVisible
    protected Boolean isFieldUpdated(SObject obj, SObjectField field){
        if (oldRecords==null || oldRecords.isEmpty()){
            return true;
        }
        //TODO: probably is better to skip error throwing
        if (newRecords==null || newRecords.isEmpty()){
            throw new TriggerHandlerException('Not allowed to use isFieldUpdated in non-update operation');
        }
        return obj.get(field)!=getFieldOldValue(obj,field);
    }

    /**
     * @param obj SObject to check. Id must present.
     * @param field field to check
     *
     * @return field value of Trigger.old object
     */
    public Object getFieldOldValue(SObject obj, SObjectField field){
        if (oldRecordsMap==null || oldRecordsMap.isEmpty()){
            return null;
        }
        return oldRecordsMap.get(obj.Id).get(field);
    }

}