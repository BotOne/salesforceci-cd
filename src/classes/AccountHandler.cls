public with sharing class AccountHandler {
    public static Account insertNewAccount(String name){
        if(name == null || String.isEmpty(name)){
            return null;
        }

        Account newAccount = new Account();
        newAccount.Name = name;

        insert newAccount;

        return newAccount;
    }
}