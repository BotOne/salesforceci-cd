public with sharing class RandomGenerateFactory {
    public static List<Account> generateAccounts(Integer count){
        List<Account> result = new List<Account>();
        for(Integer index = 0; index < count; ++index){
            Account account = new Account(Name = 'GeneratedUser_' + index);
            result.add(account);
        }
        return result;
    }

    public static List<Lead> generateLeads(Integer count){
        List<Lead> leads = new List<Lead>();
        for(Integer index = 0; index < count; ++index){
            Lead lead = new Lead(
                    Company = 'Company ' + index,
                    FirstName = 'First Name ' + index,
                    LastName = 'Last Name ' + index
            );
            leads.add(lead);
        }
        return leads;
    }

    public static List<Contact> generateContacts(Integer count, Id accountId){
        List<Contact> contacts = new List<Contact>();
        for(Integer index = 0; index < count; ++index){
            Contact contact = new Contact(
                    FirstName = 'First Name ' + index,
                    LastName = 'Last Name ' + index,
                    AccountId = accountId
            );
            contacts.add(contact);
        }
        return contacts;
    }
}