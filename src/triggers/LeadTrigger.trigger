/**
 * Created by BotOne on 27.08.2019.
 */

trigger LeadTrigger on Lead (after insert) {
    DelayController.startLeadsUpdate();
}