/**
 * Created by BotOne on 11.12.2019.
 */

trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Task> tasks = new List<Task>();
    List<Opportunity> opportunities = [SELECT Id FROM Opportunity WHERE Id IN :Trigger.new AND StageName = 'Closed Won'];

    for(Opportunity opportunity : opportunities){
        // Check old.
        if(Trigger.isInsert || (Trigger.isUpdate && Trigger.oldMap.get(opportunity.Id).StageName != 'Closed Won')){
            Task task = new Task(Subject = 'Follow Up Test Task', WhatId = opportunity.Id);
            tasks.add(task);
        }
    }
    insert tasks;
}