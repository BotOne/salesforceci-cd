({
    initMap : function ()
    {
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 4,
        center: {lat: -24.345, lng: 134.46}  // Australia.
      });

      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: true,
        map: map,
        panel: document.getElementById('right-panel')
      });

      directionsDisplay.addListener('directions_changed', function()
      {
        computeTotalDistance(directionsDisplay.getDirections());
      });

      displayRoute('Perth, WA', 'Sydney, NSW', directionsService,
          directionsDisplay);
    }
});