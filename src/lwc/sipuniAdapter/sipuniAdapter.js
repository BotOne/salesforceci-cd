import { LightningElement, track } from 'lwc';


export default class SipuniAdapter extends LightningElement {
    @track webSocket;

        connectedCallback() {
            this.openSocket();
        }

        openSocket = () => {
            // Open server socket
            if (this.webSocket !== undefined && this.webSocket.readyState !== WebSocket.CLOSED) {
                alert("WebSocket is already opened");
                return;
            }

            this.webSocket = new WebSocket("wss://wss.sipuni.com/api");

            if (this.webSocket === undefined){
                alert("Error creating socket...");
                return;
            }

            this.webSocket.onerror = function (error){
                console.log('WebSocket Error ', error)
            }

            this.webSocket.onopen = (evt) => {
                console.log('opened');
                console.log('test',this.webSocket);
                this.webSocket.send('{"type":"auth","body":{"key":"13d51303e4f9588d43a9e721893fffd2"}}');
                console.log('sended');
                alert("in onopen callback" + evt.data);
            }

            this.webSocket.onmessage = function(event){
                console.log("in onmessage callback   " + event.data);
            }

            this.webSocket.onclose = function(){
                alert("in onclose callback");
            }
        }
}