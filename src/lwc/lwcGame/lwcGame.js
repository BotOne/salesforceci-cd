import {LightningElement, track} from 'lwc';
import fpsmeter from '@salesforce/resourceUrl/fpsmeter';
import background from '@salesforce/resourceUrl/background';
import atlas from '@salesforce/resourceUrl/elements';
import hitSound from '@salesforce/resourceUrl/hitSound';
import { loadScript } from "lightning/platformResourceLoader";

export default class LwcGame extends LightningElement {

    canvas;
    ctx;
    timerId;
    @track showGame = false;

    buttonLabel;
    infoText;

    //Ball
    ballStartX;
    ballStartY;
    ballRadius = 20;
    dx = 200;
    dy = -200;

    //Paddle
    paddleX;
    paddleHeight = 17;
    paddleWidth = 75;

    //Bricks
    brickRowCount = 7;
    brickColumnCount = 9;
    brickWidth = 75;
    brickHeight = 35;
    brickPadding = 10;
    brickOffsetTop = 30;
    brickOffsetLeft = 15;
    bricks = [];

    //Movement
    rightPressed = false;
    leftPressed = false;
    drag;

    //Player score and lives
    score;
    lives;

    //Core
    now;
    deltaTime;
    last;

    //Utils
    meter;

    //Resources
    spriteImage;
    spriteSheet = atlas;

    audioElement;
    audioSource = hitSound;

    backgroundImage;
    backgroundSource = background;

    connectedCallback() {
        this.showInfoWindow('Начать игру?','ОК', this.startGame);
        loadScript(this, fpsmeter ).then(() => {
            this.canvas = this.template.querySelector("canvas");
            this.meter = new FPSMeter({ decimals: 0, graph: true, theme: 'dark', left: '5px', top: '5px' });
            this.ctx = this.canvas.getContext('2d');
            this.now = this.timestamp();
            this.last = this.now;

            this.resetPosition();

            this.spriteImage = document.createElement('img');
            this.spriteImage.src = this.spriteSheet;

            this.backgroundImage = document.createElement('img');
            this.backgroundImage.src = this.backgroundSource;

            this.audioElement = document.createElement("audio");
            this.audioElement.src = this.audioSource;

            requestAnimationFrame(this.frame);
            console.log("Script fpsmeter...");
        });
    }

    startGame = () => {
        this.showGame = true;
        this.lives = 1;
        this.score = 0;
    }


    renderedCallback() {

        for (var c = 0; c < this.brickColumnCount; c++) {
            this.bricks[c] = [];
            for (var r = 0; r < this.brickRowCount; r++) {
                this.bricks[c][r] = {x: 0, y: 0, status: 1};
            }
        }

        document.addEventListener("keydown", this.keyDownHandler, false);
        document.addEventListener("keyup", this.keyUpHandler, false);
        document.addEventListener('mousedown', this.mouseDownHandler, false);
        document.addEventListener('mouseup', this.mouseUpHandler, false);
        document.addEventListener("mousemove", this.mouseMoveHandler, false);
    }

    // Utils functions
    valueInRange = (value, min, max) => {
        return (value >= min) && (value <= max);
    }

    ovr = (rectA, rectB) => {
        var xOverlap = this.valueInRange(A.x, B.x, B.x + B.width) ||
                        this.valueInRange(B.x, A.x, A.x + A.width);

        var yOverlap = this.valueInRange(A.y, B.y, B.y + B.height) ||
                        this.valueInRange(B.y, A.y, A.y + A.height);

        return xOverlap && yOverlap;
    }

    // Core functions
    timestamp = () => {
      return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
    }

    frame = () => {
        this.meter.tickStart();

        this.now = this.timestamp();
        this.deltaTime = (this.now - this.last) / 1000;    // duration in seconds

        this.update(this.deltaTime);
        this.render(this.deltaTime, this.ctx);

        this.last = this.now;

        this.meter.tick();

        requestAnimationFrame(this.frame);
    }

    update = (deltaTime) => {
        this.ballStartX += this.dx;
        this.ballStartY += this.dy;

        this.collisionDetection();

        if (this.ballStartX + this.dx > this.canvas.width - this.ballRadius || this.ballStartX + this.dx < this.ballRadius) {
            this.dx = -this.dx;
        }

        if (this.ballStartY + this.dy < this.ballRadius) {
            this.dy = -this.dy;
        } else if (this.ballStartY + this.dy > this.canvas.height - this.ballRadius) {
            if (this.ballStartX > this.paddleX && this.ballStartX < this.paddleX + this.paddleWidth) {
                this.dy = -this.dy;
            } else {
                this.lives--;
                if (!this.lives) {
                    //clearInterval(this.timerId); // Needed for Chrome to end game
                    this.showInfoWindow('Вы проиграли','Попробовать еще раз', this.startGame);

                } else {
                    this.resetPosition();
                }

            }
        }

        if (this.rightPressed) {
            this.paddleX += 7;
            if (this.paddleX + this.paddleWidth > this.canvas.width) {
                this.paddleX = this.canvas.width - this.paddleWidth;
            }
        } else if (this.leftPressed) {
            this.paddleX -= 7;
            if (this.paddleX < 0) {
                this.paddleX = 0;
            }
        }
    }

    render = (deltaTime) => {
        if(this.ctx == null || this.ctx == undefined) return;

        this.drawBackground(this.ctx);
        this.drawBrick(this.ctx);
        this.drawBall(this.ctx);
        this.drawPaddle(this.ctx);
        this.drawScore(this.ctx);
        this.drawLives(this.ctx);
    }


    //Movement handlers

    keyDownHandler = (e) => {
        if (e.key == "Right" || e.key == "ArrowRight") {
            this.rightPressed = true;
        } else if (e.key == "Left" || e.key == "ArrowLeft") {
            this.leftPressed = true;
        }
    }

    keyUpHandler = (e) => {
        if (e.key == "Right" || e.key == "ArrowRight") {
            this.rightPressed = false;
        } else if (e.key == "Left" || e.key == "ArrowLeft") {
            this.leftPressed = false;
        }
    }

    mouseDownHandler = () => {
        this.drag = true;
    }

    mouseUpHandler = () => {
        this.drag = false;
    }

    mouseMoveHandler = (e) => {
        if (this.drag) {
            var relativeX = e.clientX - this.canvas.offsetLeft;
            if (relativeX > 0 && relativeX < this.canvas.width) {
                this.paddleX = relativeX - this.paddleWidth / 2;
            }
        }
    }

    //draw functions
    drawBackground = (ctx) => {
        ctx.drawImage(this.backgroundImage,0,0,this.canvas.width,this.canvas.height);
        //ctx.drawImage(this.background,0,0, this.canvas.width, this.canvas.height, this.canvas.width, this.canvas.height);
    }

    drawBrick = (ctx) => {
        for (var c = 0; c < this.brickColumnCount; c++) {
            for (var r = 0; r < this.brickRowCount; r++) {
                if (this.bricks[c][r].status == 1) {
                    var brickX = (c * (this.brickWidth + this.brickPadding)) + this.brickOffsetLeft;
                    var brickY = (r * (this.brickHeight + this.brickPadding)) + this.brickOffsetTop;
                    this.bricks[c][r].x = brickX;
                    this.bricks[c][r].y = brickY;

                    ctx.drawImage(this.spriteImage,10,112,351,112,brickX,brickY,this.brickWidth,this.brickHeight);
                }
            }
        }
    }

    drawPaddle = (ctx) => {
        ctx.drawImage(this.spriteImage,10,234,362,93,this.paddleX,this.canvas.height - this.paddleHeight,this.paddleWidth,this.paddleHeight);
    }

    drawBall = (ctx) => {
        ctx.drawImage(this.spriteImage,10,10,103,92,this.ballStartX,this.ballStartY,this.ballRadius,this.ballRadius);
    }

    drawScore = (ctx) => {
        ctx.font = "16px Arial";
        ctx.fillStyle = "#0095DD";
        ctx.fillText("Score: " + this.score, 8, 20);
    }

    drawLives = (ctx) => {
        ctx.font = "16px Arial";
        ctx.fillStyle = "#0095DD";
        ctx.fillText("Lives: " + this.lives, this.canvas.width - 65, 20);
    }
    //check collisions

    collisionDetection = () => {
        for (var c = 0; c < this.brickColumnCount; c++) {
            for (var r = 0; r < this.brickRowCount; r++) {
                var b = this.bricks[c][r];
                if (b.status == 1) {
                    if (this.ballStartX > b.x && this.ballStartX < b.x + this.brickWidth && this.ballStartY > b.y && this.ballStartY < b.y + this.brickHeight) {

                        this.audioElement.play();

                        this.dy = -this.dy;
                        b.status = 0;
                        this.score += 10;
                        if (this.score/10 == this.brickRowCount * this.brickColumnCount) {
                            //clearInterval(this.timerId); // Needed for Chrome to end
                            this.showInfoWindow('Вы Выиграли','Сыграть заново', this.startGame);
                        }
                    }
                }
            }
        }
    }

    resetPosition = () => {
        this.ballStartX = this.canvas.width / 2;
        this.ballStartY = this.canvas.height - 30;
        this.dx = 2;
        this.dy = -2;
        this.paddleX = (this.canvas.width - this.paddleWidth) / 2;
    }

    //UI Window
    showInfoWindow = (infoText, buttonText, someMethod) => {
        this.showGame = false;
        this.infoText = infoText;
        this.buttonLabel = buttonText;
        this.buttonLogic = someMethod;
    }
}